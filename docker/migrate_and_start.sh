# Try to run migrate up to 5 times with a 5 second break
# This is to allow postgres to start
{
  sleep 5
  n=0
  until [ $n -ge 5 ]
  do
    flyway migrate && break
    n=$[$n+1]
    sleep 5
  done
}&

docker-entrypoint.sh postgres
