from typing import Optional

from app.database import User, get_session


def find_user_by_name(name: str) -> Optional[User]:
    # Users are unique by name, should only return none or one
    return get_session().query(User).filter(User.name == name).first()
