from app.database.base import Base


class Category(Base):
    def __repr__(self) -> str:
        return f"{self.name}: {self.description}"

    __tablename__ = "categories"


class Tag(Base):
    def __repr__(self) -> str:
        return f"{self.name} ({self.tag_category}): {self.description}"

    __tablename__ = "tags"
