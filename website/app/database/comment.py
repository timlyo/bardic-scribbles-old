from app import forms
from .base import Base


class Comment(Base):
    __tablename__ = "comments"

    @classmethod
    def from_form(cls, form: forms.Comment, on_scribble: str, poster: str, parent=None):
        return cls(
            poster=poster,
            on_scribble=on_scribble,
            parent=parent,
            content=form.content.data
        )
