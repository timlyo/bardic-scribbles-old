from app import forms
from .base import Base


class Scribble(Base):
    __tablename__ = "scribbles"

    @classmethod
    def from_form(cls, form: forms.ScribbleEdit):
        return cls(
            title=form.title.data,
            description=form.description.data,
            scribble_category=form.category.data
        )

    def short_desc(self):
        if len(self.description) < 81:
            return self.description
        else:
            return self.description[:77] + "..."

    def tags(self):
        return self.taglist_collection
