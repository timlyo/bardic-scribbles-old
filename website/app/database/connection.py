import os
import sys

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker, scoped_session

# Must be imported here to enable reflection
from .user import User  # NOQA
from .scribble import Scribble  # NOQA
from .comment import Comment  # NOQA
from .tag_list import TagList  # NOQA
from .base import Base

Session = None


def get_session() -> sqlalchemy.orm.session.Session:
    return Session()


def add_and_refresh(item, session=None):
    """Convenience method that adds the item to the database and updates it's record. Handles session creation"""
    if session is None:
        session = get_session()

    session.add(item)
    session.commit()
    session.refresh(item)
    return item


def connect_to_database(host):
    global Session, TagList

    print(f"Connecting to database at {host}")

    engine = create_engine(
        f"postgresql://scanlan@{host}:5432/bardic_scribbles",
        connect_args={"connect_timeout": 20},
        pool_size=20
    )

    Base.prepare(engine, reflect=True)

    Session = scoped_session(sessionmaker(bind=engine))


def do_janky_scan_thing():
    # Automatic scanning for CI pipeline, remove as soon as possible (see issue #1)
    print("falling back to automatic database scanning")

    # Try 3 times in case database has started slowly
    for tries in range(3):
        for address in range(10):
            try:
                connect_to_database(f"172.17.0.{address}")
                return

            except OperationalError:
                continue


try:
    host = os.environ.get("DATABASE_HOST", "localhost")
    connect_to_database(host)

except OperationalError:
    do_janky_scan_thing()

finally:
    if Session is not None:
        print("Connected to database")
    else:
        print("Database connection failed", file=sys.stderr)
