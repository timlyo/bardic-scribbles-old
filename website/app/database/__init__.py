from typing import List

# Define database objects
from sqlalchemy.orm import joinedload

from .user import User  # NOQA
from .scribble import Scribble  # NOQA
from .comment import Comment  # NOQA
from .tag import Category, Tag  # NOQA

from functools import wraps

from .connection import get_session, add_and_refresh
from . import connection, util

__all__ = [add_and_refresh, get_session, util]

if connection.Session is None:
    exit(-1)


def with_session(func):
    """Decorator for routes that need a database session"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        session = get_session()
        kwargs["session"] = session

        try:
            result = func(*args, **kwargs)
            return result
        finally:
            session.close()

    return wrapper


def form_comment_tree(comments: List) -> List[Comment]:
    # Index of comment ids for lookup
    index = dict()

    # index all comments
    for comment in comments:
        comment.__setattr__("children", [])
        index[comment.id] = comment

    # Add all children to parents in index
    for orphan in comments:
        if orphan.parent is not None:
            index[orphan.parent].children.append(orphan)

    return [comment for comment in comments if comment.parent is None]


def load_scribble_comment_tree(scribble_id: str, session):
    comments = session.query(Comment).options(joinedload(Comment.user)).filter(Comment.on_scribble == scribble_id)
    return form_comment_tree(comments)
