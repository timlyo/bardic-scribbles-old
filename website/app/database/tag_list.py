from app.database.base import Base


class TagList(Base):
    __tablename__ = "tag_list"

    def __repr__(self):
        return self.name
