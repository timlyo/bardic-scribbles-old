from flask_login import UserMixin

from app import forms
from .base import Base
from sqlalchemy import Enum
from passlib.hash import argon2


class AccountType(Enum):
    user = "user"
    admin = "admin"
    moderator = "moderator"


class User(UserMixin, Base):
    __tablename__ = "users"

    def is_active(self):
        return True

    def get_id(self):
        return self.id

    def test_password(self, password: str) -> bool:
        return argon2.verify(password, self.password)

    @classmethod
    def from_form(cls, form: forms.NewUser):
        return cls(
            name=form.username.data,
            password=argon2.hash(form.password.data),
            email=form.email.data,
            account_type=AccountType.user
        )
