from flask import Flask
from flask_login import LoginManager
from app import views, database
import os

run_mode = os.environ.get("RUN_MODE", "dev")


def set_secret_key():
    if run_mode == "dev":
        app.logger.warning("Getting development secret key")
        app.secret_key = os.environ.get("SECRET_KEY", "dev-secret-key")
    else:
        app.secret_key = os.environ.get("SECRET_KEY")
        assert app.secret_key is not None, "SECRET_KEY must be defined when not in dev mode"


app = Flask(__name__)
set_secret_key()
app.register_blueprint(views.pages)

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(id):
    session = database.get_session()
    user = session.query(database.User).get({"id": id})
    session.close()
    return user
