from sqlalchemy import and_
from sqlalchemy.exc import IntegrityError
from flask import Blueprint, render_template, redirect, flash, current_app, abort, request, url_for
from flask_login import login_user, current_user, logout_user, login_required
from sqlalchemy.orm import joinedload

from app import util
from app import database, forms
from app.database import Scribble, Comment, Tag, Category, with_session, User
from app.database.tag_list import TagList

pages = Blueprint("pages", __name__,
                  template_folder="templates")


@pages.route('/')
@with_session
def index(session):
    scribbles = session.query(database.Scribble).limit(10)

    return render_template("index.html", scribbles=scribbles)


@pages.route("/new_user", methods=["GET"])
def new_user_page():
    form = forms.NewUser()

    return render_template("new_user.html", form=form)


@pages.route("/new_user", methods=["POST"])
def create_new_user():
    form = forms.NewUser()
    if form.validate():
        user = database.User.from_form(form)
        if database.util.find_user_by_name(user.name) is None:
            current_app.logger.info(f"Creating new user '{form.username.data}'")
            database.add_and_refresh(user)
            login_user(user)
            return redirect(f"/user/{user.id}")
        else:
            flash("Username is already taken", "warning")
    return render_template("new_user.html", form=form)


@pages.route("/login", methods=["GET"])
def login_page():
    if current_user.is_authenticated:
        return redirect("/")

    form = forms.LoginForm()
    return render_template("login.html", form=form)


@pages.route("/login", methods=["POST"])
def login():
    form = forms.LoginForm()
    if form.validate():
        session = database.get_session()
        user = session.query(database.User).filter(database.User.name == form.username.data).first()
        if user is None:
            flash("Invalid logon details")
            return render_template("login.html", form=form)

        if not user.test_password(form.password.data):
            flash("Incorrect password")
            return render_template("login.html", form=form)

        login_user(user)
        current_app.logger.info(f"User logged in. id: {form.username.data}")
        return redirect(f"/user/{user.id}")
    else:
        flash("Invalid form")
        return render_template("login.html", form=form)


@pages.route("/logout", methods=["GET", "POST"])
@login_required
def logout():
    current_app.logger.info("Logged out user")
    logout_user()
    return redirect("/")


@pages.route("/user/<id>", methods=["GET"])
@with_session
def user_page(session, id: str):
    user = session.query(User)\
        .get(id)

    if user is None:
        abort(404)

    top_scribbles = session.query(Scribble)\
        .options(joinedload(Scribble.taglist_collection))\
        .filter(Scribble.creator_id == id).all()

    latest_comments = session.query(database.Comment) \
        .filter(Comment.poster == id) \
        .order_by(Comment.time_posted) \
        .limit(10)

    return render_template("user.html",
                           user=user,
                           top_scribbles=top_scribbles,
                           latest_comments=list(latest_comments),
                           title=f"{user.name}'s profile")


@pages.route("/admin", methods=["GET"])
@with_session
def admin(session):
    # Allow all access in debug mode, otherwise hide if not admin
    if current_app.debug is not True and current_user.account_type != "admin":
        # Abort with 404 to hide from crawlers
        abort(404)

    users = session.query(database.User)
    categories = session.query(database.Category).all()

    return render_template("admin.html", users=users, categories=categories)


@pages.route("/new_scribble", methods=["GET"])
@with_session
def new_scribble_page(session):
    categories = session.query(database.Category).all()
    form = forms.ScribbleEdit(categories)

    return render_template("edit_scribble.html", form=form)


@pages.route("/new_scribble", methods=["POST"])
@with_session
def new_scribble(session):
    categories = session.query(database.Category).all()
    form = forms.ScribbleEdit(categories)
    if form.validate():
        new_scribble = Scribble.from_form(form)

        new_scribble.creator_id = current_user.id
        current_app.logger.info(f"Creating scribble called {new_scribble.title}")
        database.add_and_refresh(new_scribble)

        return redirect(f"/scribble/{new_scribble.id}")
    else:
        return render_template("edit_scribble.html", form=form)


@pages.route("/scribble/<id>", methods=["GET"])
@with_session
def show_scribble(session, id):
    scribble = session.query(Scribble)\
        .options(joinedload(Scribble.user))\
        .options(joinedload(Scribble.taglist_collection))\
        .get(id)

    if not scribble:
        abort(404)

    comments = database.load_scribble_comment_tree(id, session)
    new_comment_form = forms.Comment()

    if current_user.id == scribble.creator_id:
        is_creator = True
        valid_tags = session.query(Tag).filter(Tag.tag_category == scribble.scribble_category)
    else:
        is_creator = False
        valid_tags = []

    return render_template("scribble.html",
                           scribble=scribble,
                           new_comment_form=new_comment_form,
                           comments=comments,
                           title=scribble.title,
                           is_creator=is_creator,
                           valid_tags=valid_tags)


@pages.route("/comment/<scribble_id>", methods=["POST"])
@login_required
@with_session
def new_comment(scribble_id, session):
    form = forms.Comment()
    user = current_user
    parent = request.args.get("parent")

    if form.validate():
        comment = Comment.from_form(form, scribble_id, user.id, parent=parent)
        current_app.logger.info("Posting a new comment")
        database.add_and_refresh(comment, session)
        return redirect(f"/scribble/{scribble_id}")
    else:
        return form.errors


@pages.route("/edit_comment/<comment_id>", methods=["GET"])
@login_required
@with_session
def edit_comment(comment_id, session):
    comment = session.query(Comment).get(comment_id)

    if comment.poster != current_user.id:
        flash("Error. Cannot edit another user's comment")
        return redirect("/")

    form = forms.Comment()
    form.content.data = comment.content

    return render_template("edit_comment.html", form=form, comment=comment, parent=comment.parent)


@pages.route("/edit_comment/<comment_id>", methods=["PUT"])
@with_session
def put_comment(session):
    form = forms.Comment()
    if form.validate():
        comment = Comment.from_form(form)
        database.add_and_refresh(comment, session)
    else:
        return form.errors


@pages.route("/reply/<parent_id>", methods=["GET"])
@login_required
@with_session
def add_reply(parent_id, session):
    parent = session.query(Comment).get(parent_id)
    form = forms.Comment()

    return render_template("edit_comment.html", form=form, parent=parent)


@pages.route("/add_tag/<scribble_id>", methods=["GET"])
@login_required
@with_session
def add_tag_page(scribble_id, session):
    util.check_user_owns_scribble(scribble_id, session)

    scribble = session.query(Scribble).get(scribble_id)

    possible_tags = session.query(Tag).filter(Tag.tag_category == scribble.scribble_category)
    selected_tags_names = [tag.name for tag in session.query(TagList).filter(TagList.scribble_id == scribble_id).all()]

    return render_template("add_tag.html", scribble=scribble,
                           possible_tags=possible_tags, selected_tags_names=selected_tags_names)


@pages.route("/add_tag/<scribble_id>/<name>/<action>", methods=["POST"])
@login_required
@with_session
def add_tag(scribble_id, name, action, session):
    util.check_user_owns_scribble(scribble_id, session)

    try:
        if action == "add":
            new_tag = TagList(scribble_id=scribble_id, name=name, submitter=current_user.id)
            database.add_and_refresh(new_tag, session)
        elif action == "delete":
            session.query(TagList) \
                .filter(and_(TagList.scribble_id == scribble_id, TagList.name == name)) \
                .delete()
            session.commit()
        else:
            flash("Invalid Action")

    except IntegrityError as e:
        flash(str(e))

    return redirect(url_for("pages.add_tag_page", scribble_id=scribble_id))


@pages.route("/search", methods=["POST"])
def search():
    # WTFORMs can't handle a fieldlist of boolean field, therfor the form must be parsed manually :(

    parameters = []
    form = request.form
    category = form.get("category")

    tags = [
        tag for (tag, value) in form.items()
        if tag.startswith("tag-") and value == "on"]

    if category:
        parameters.append("category=" + category)

    if tags:
        parameters += ["tag=" + tag.replace("tag-", "") for tag in tags]

    return redirect(f"/search?{'&'.join(parameters)}")


@pages.route("/search", methods=["GET"])
@with_session
def search_page(session):
    category = request.args.get("category")
    tags = request.args.getlist("tag")

    scribbles = session.query(Scribble).outerjoin(TagList)

    if category is not None:
        scribbles = scribbles.filter(Scribble.scribble_category == category)

    if tags:
        scribbles = scribbles.filter(TagList.name.in_(tags))

    scribbles = scribbles.distinct()

    categories = session.query(Category).all()
    valid_tags = session.query(Tag).filter(Tag.tag_category == category)

    print(category)

    return render_template("search.html",
                           scribbles=scribbles,
                           selected_category=category,
                           categories=categories,
                           tags=valid_tags,
                           ticked=tags)
