from typing import List

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, TextAreaField, SelectField, FieldList, BooleanField
from wtforms.validators import DataRequired, Email


class NewUser(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    email = StringField("Email", validators=[Email(), DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])


class ScribbleEdit(FlaskForm):
    title = StringField("Title", validators=[DataRequired()])
    description = TextAreaField("Description", validators=[DataRequired()])
    category = SelectField("Category", validators=[DataRequired()])

    def __init__(self, categories: List):
        super().__init__()
        self.category.choices = [(c.name, c.name) for c in categories]
        # Add a blank entry first
        self.category.choices.insert(0, ("", ""))


class Comment(FlaskForm):
    content = TextAreaField("Comment", validators=[DataRequired()])


class Search(FlaskForm):
    tags = FieldList(BooleanField())

    def __init__(self, tags: List[str]):
        super().__init__()
        self.tags.entries = [BooleanField(tag) for tag in tags]
