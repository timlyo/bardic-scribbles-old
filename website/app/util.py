from flask import flash
from flask_login import current_user

from app import database
from app.database import Scribble


def check_user_owns_scribble(scribble_id: str, database_session=None):
    if database_session is None:
        database_session = database.get_session()

    scribble = database_session.query(Scribble).get(scribble_id)

    if scribble.creator_id != current_user.id:
        flash("Error cannot edit other user's tags")
        raise Exception("Error. Incorrect user")
