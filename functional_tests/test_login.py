import random

import util
from init import driver, base_url

login_url = f"{base_url}/login"
new_user_url = f"{base_url}/new_user"


def test_login(driver):
    rand_id = random.randint(0, 1204)
    util.create_test_user(driver, f"login_{rand_id}", logout=True)

    driver.get(login_url)
    form = driver.find_element_by_id("login-form")
    form.find_element_by_id("username").send_keys(f"test_user_login_{rand_id}")
    form.find_element_by_id("password").send_keys("test-password")
    form.find_element_by_id("login-button").click()

    if "/user/" not in driver.current_url:
        util.get_errors_and_fail(driver)


def test_login_fails_with_wrong_password(driver):
    rand_id = random.randint(0, 1204)
    util.create_test_user(driver, f"login_{rand_id}", logout=True)

    driver.get(login_url)
    form = driver.find_element_by_id("login-form")
    form.find_element_by_id("username").send_keys(f"test_user_login_{rand_id}")
    form.find_element_by_id("password").send_keys("blargle-flarty-whoop")
    form.find_element_by_id("login-button").click()

    # Check no redirect happened
    if "/login" not in driver.current_url:
        util.get_errors_and_fail(driver)

    messages = util.get_page_messages(driver)
    assert "Incorrect password" in messages
