import util
from init import *

create_url = f"{base_url}/new_scribble"


def create_scribble(driver, title, description, category):
    driver.get(create_url)

    form = driver.find_element_by_id("new-scribble-form")
    form.find_element_by_id("title").send_keys(title)
    form.find_element_by_id("description").send_keys(description)

    categories = form.find_element_by_id("category").find_elements_by_tag_name("option")
    [c for c in categories if c.text == category][0].click()

    form.find_element_by_id("save-button").click()


def test_post_character(driver):
    util.create_test_user(driver)

    create_scribble(driver, "Test Character", "Bit of a twat really", "Character")
    assert "/scribble/" in driver.current_url


def test_post_event(driver):
    util.create_test_user(driver)

    create_scribble(driver, "Test Event", "Things done gone wrong", "Event")
    assert "/scribble/" in driver.current_url


def test_post_item(driver):
    util.create_test_user(driver)

    create_scribble(driver, "Test Item", "Wonga?", "Item")
    assert "/scribble/" in driver.current_url


def test_post_other(driver):
    util.create_test_user(driver)

    create_scribble(driver, "Test Other", "Wot dat?", "Other")
    assert "/scribble/" in driver.current_url


def test_post_world(driver):
    util.create_test_user(driver)

    create_scribble(driver, "Test world", "Queue Aladdin", "World")
    assert "/scribble/" in driver.current_url


def test_post_character_and_tag_bbeg(driver):
    util.create_test_user(driver)

    create_scribble(driver, "Test BBEG Character", "Massive Twat", "Character")

    driver.find_element_by_id("modify-tags").click()
    driver.find_element_by_id("BBEG").click()
    driver.find_element_by_id("back").click()

    driver.find_element_by_id("tag-BBEG")
