import random

import util
from init import driver, base_url

new_user_url = f"{base_url}/new_user"


def test_create_new_user(driver):
    rand_id = random.randint(0, 1024)
    driver.get(new_user_url)

    util.create_test_user(driver, rand_id)

    # Check redirected to user page
    if "/user/" not in driver.current_url:
        util.get_errors_and_fail(driver)

    assert driver.find_element_by_id("username").text == f"test_user_{rand_id}"


def test_duplicate_usernames_creates_error(driver):
    rand_id = random.randint(0, 1024)
    driver.get(new_user_url)

    util.create_test_user(driver, rand_id)

    # Check redirected to user page
    if "/user/" not in driver.current_url:
        util.get_errors_and_fail(driver)

    driver.get(new_user_url)

    util.create_test_user(driver, rand_id)

    # no redirect for second creation
    assert "/new_user" in driver.current_url

    # Check messages contains message about username being taken
    messages = util.get_page_messages(driver)
    assert "Username is already taken" in messages
