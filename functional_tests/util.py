import sys
from random import randint
from typing import List, Optional

import pytest
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from init import base_url


def get_new_user_creation_form(driver):
    driver.get(f"{base_url}/new_user")
    return driver.find_element_by_id("new-user-form")


def create_test_user(driver, id=None, logout=False):
    if id is None:
        id = randint(0, 10000)

    print("Creating user", id)

    user_form = get_new_user_creation_form(driver)
    user_form.find_element_by_id("username").send_keys(f"test_user_{id}")
    user_form.find_element_by_id("email").send_keys(f"test-email_{id}@test.com")
    user_form.find_element_by_id("password").send_keys("test-password")
    user_form.find_element_by_id("create-button").click()

    # sleep(1)

    if logout:
        driver.find_element_by_id("logout-button").click()

    print("Created")

    return id


def get_page_messages(driver) -> Optional[List[str]]:
    try:
        messages = driver.find_element_by_id("messages").find_elements_by_class_name("notification")
        return [message.text for message in messages]
    except NoSuchElementException:
        return None


def get_errors_and_fail(driver):
    page = driver.current_url
    messages = get_page_messages(driver)
    title = driver.title

    if messages:
        pytest.fail(f"Test failed on {page}:{title}. {messages}")
    else:
        print(driver.page_source, file=sys.stderr)
        pytest.fail(f"Test failed on {page}:{title}. Couldn't find errors")


def assert_logged_out(driver):
    if driver.find_element_by_id("login-button") is None:
        pytest.fail(f"Couldn't find login-button on {driver.curent_url}")
