from selenium.webdriver.support.ui import Select

import util
from init import driver, base_url


def test_load_user_page_with_post_and_comments(driver):
    """Load the page of a user who has posted and commented"""

    util.create_test_user(driver)

    driver.find_element_by_id("username").click()

    # Make post
    driver.find_element_by_id("new-scribble-button").click()

    driver.find_element_by_id("title").send_keys("test")
    driver.find_element_by_id("description").send_keys("test")
    Select(driver.find_element_by_id("category")).select_by_visible_text("Item")
    driver.find_element_by_id("save-button").click()

    # Make comments on post
    driver.find_element_by_id("comment-content").send_keys("ur a newb, butthead")
    driver.find_element_by_id("post-comment-button").click()

    # Check page
    driver.find_element_by_id("username").click()

    assert "Bardic Scribbles" in driver.title


