from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import pytest
import os

webhost = os.environ.get("WEBHOST", "localhost")
selenium_host = os.environ.get("SELENIUM_HOST", "localhost")
base_url = f"http://{webhost}:5000"

driver_name = os.environ.get("DRIVER", "firefox")

print(f"webhost={webhost}, selenium_host={selenium_host}, driver_name={driver_name}")


@pytest.fixture(scope="function")
def driver(request):
    options = Options()
    options.headless = True

    if os.environ.get("CI") is not None:
        driver = webdriver.Remote(
            command_executor=f"http://{selenium_host}:4444/wd/hub",
            desired_capabilities=DesiredCapabilities.FIREFOX,
            options=options)
    else:
        if driver_name == "firefox":
            driver = webdriver.Firefox(options=options)
        elif driver_name == "chrome":
            driver = webdriver.Chrome(options=options)
        else:
            raise NameError(f"driver {driver_name} not recognised")

    try:
        yield driver
    finally:
        if os.environ.get("CI") is None:
            screen_shot(driver)
        driver.close()


def screen_shot(driver):
    if not os.path.exists("screenshots"):
        os.makedirs("screenshots")
    name = os.environ.get("PYTEST_CURRENT_TEST")
    assert driver.save_screenshot(f"screenshots/{name}.png")
