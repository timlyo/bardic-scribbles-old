from threading import Thread
import requests

from init import driver, base_url


def test_simple_request():
    requests.get(base_url).raise_for_status()


def test_homepage_has_correct_title(driver):
    print(f"Connecting to {base_url}")
    driver.get(base_url)

    assert "Bardic Scribbles" == driver.title


def test_loading_homepage():
    threads = [Thread(target=lambda: requests.get(base_url))
               for _ in range(50)]

    for thread in threads:
        thread.start()
        
    for thread in threads:
        thread.join()
