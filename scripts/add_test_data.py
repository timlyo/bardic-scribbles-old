"""Creates a bunch of test data in the postgres database"""
import sys

sys.path.append("website/")

from app.database import *

# (username, password)
users = [["Alice", "alice-password"], ["Bob", "bob-password"]]
# (creator_index, title, content)
scribbles = [
    [0, "test_scribble1", "test_description"],
    [0, "test_scribble2", "test_description"],
    [1, "test_scribble3", "test_description"],
]
# (scribble_index, parent_index, content)
comments = [
    [0, None, "test_comment"],
    [0, 0, "reply to test comment"],
    [0, 1, "Reply to reply"],
    [1, None, "test_comment_2"]
]


def create_users():
    """Creates users and updates user list with database elements"""
    for index, user in enumerate(users):
        print(f"Adding {user[0]}")
        new_user = User(name=user[0], password=user[1])
        session.add(new_user)
        session.flush()
        # Refresh updates the new_user with the generated id
        session.refresh(new_user)
        users[index] = new_user


def create_scribbles():
    for index, scribble in enumerate(scribbles):
        print(f"Adding scribble {scribble[1]}")
        creator = users[scribble[0]].id
        new_scribble = Scribble(creator=creator, title=scribble[1], description=scribble[2])
        session.add(new_scribble)
        session.flush()
        session.refresh(new_scribble)
        scribbles[index] = new_scribble


def create_comments():
    for index, comment in enumerate(comments):
        print(f"Adding comment {comment[2]}")
        if comment[1] is not None:
            parent = comments[comment[1]].id
        else:
            parent = None

        new_comment = Comment(scribble=scribbles[comment[0]].id, parent=parent, content=comment[2])
        session.add(new_comment)
        session.flush()
        session.refresh(new_comment)
        comments[index] = new_comment


if __name__ == '__main__':
    print("Adding test data")
    create_users()
    create_scribbles()
    create_comments()

    session.commit()
