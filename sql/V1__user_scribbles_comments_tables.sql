CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE USER scanlan;

CREATE TYPE account_type_enum AS ENUM('user', 'admin', 'moderator');

-- All timestamps are without timezone and must be UTC

CREATE TABLE users(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(77) NOT NULL,
    email VARCHAR(100) NOT NULL,
    account_type account_type_enum NOT NULL,
    created_on timestamp without time zone NOT NULL default (now() at time zone 'utc'),
    last_logged_in timestamp NOT NULL default (now() at time zone 'utc')
);

CREATE TABLE categories(
    name VARCHAR PRIMARY KEY,
    description VARCHAR
);

CREATE TABLE scribbles(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    creator_id UUID REFERENCES users(id) NOT NULL,
    title VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    time_created timestamp without time zone NOT NULL default (now() at time zone 'utc'),
    scribble_category VARCHAR REFERENCES categories(name) NOT NULL
);

CREATE TABLE comments(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    poster UUID NOT NULL REFERENCES users(id),
    on_scribble UUID NOT NULL REFERENCES scribbles(id),
    parent UUID,
    content VARCHAR NOT NULL,
    time_posted timestamp without time zone NOT NULL default (now() at time zone 'utc')
);


CREATE TABLE tags(
    name VARCHAR PRIMARY KEY,
    description VARCHAR,
    tag_category VARCHAR REFERENCES categories(name) NOT NULL
);

-- Mapping of scribble_id to tag
CREATE TABLE tag_list(
    scribble_id UUID REFERENCES scribbles(id),
    name VARCHAR REFERENCES tags(name) NOT NULL,
    submitter UUID REFERENCES users(id) NOT NULL,
    PRIMARY KEY (scribble_id, name)
);

GRANT INSERT,SELECT,UPDATE ON TABLE users TO scanlan;
GRANT INSERT,SELECT,UPDATE ON TABLE scribbles TO scanlan;
GRANT INSERT,SELECT,UPDATE ON TABLE comments TO scanlan;
GRANT SELECT ON TABLE categories TO scanlan;
GRANT SELECT ON TABLE tags TO scanlan;
GRANT INSERT,SELECT,UPDATE,DELETE ON TABLE tag_list TO scanlan;

INSERT INTO categories(name, description) VALUES
    ('Character', 'A Character for a fantasy world'),
    ('Event', 'Something interesting'),
    ('Item', 'Interesting item for adventurers to use'),
    ('Other', 'For the things that don''t quite fit in'),
    ('World', 'Details about the world around the adventure');

INSERT INTO tags(tag_category, name, description) VALUES
    ('Character', 'BBEG', 'The Big Bad Evil Guy for a campaign'),
    ('Character', 'Evil', NULL),
    ('Character', 'Historical', 'A Legend from the days of old'),
    ('Character', 'Player', NULL),
    ('Character', 'Shop Keeper', NULL),
    ('Event', 'Dungeon', NULL),
    ('Event', 'Encounter', NULL),
    ('Event', 'Fetch', NULL),
    ('Event', 'Monster Slayer', NULL),
    ('Event', 'Mystery', NULL),
    ('Event', 'Politics', NULL),
    ('Event', 'Stealth', NULL),
    ('Item', 'Armour', NULL ),
    ('Item', 'Magical', NULL ),
    ('Item', 'Potion', NULL ),
    ('Item', 'Utility', NULL),
    ('Item', 'Weapon', NULL ),
    ('World', 'Culture', NULL),
    ('World', 'Geography', NULL),
    ('World', 'Legend', NULL),
    ('World', 'Race', NULL);
