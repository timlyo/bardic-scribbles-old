# Bardic Scribbles

A website to share Tabletop RPG ideas.

# Contributing

## Running

Start a postgres database (see Database for more details) and run `run.py` under python 3.7. The Development version should then be live under `localhost:5000`

## Database

The project is based on Postgres 11. A raw postgres database can be run with a `bardic_scribbles` database added or a docker image can be used. The dockerfile with the required database added is contained under `docker`.

Flyway is used to manage database versions. This can be downloaded from [their website](https://flywaydb.org/) and the database can then be setup by running `flyway migrate`. This will apply all scripts within the sql directory in order depending on their version.

The database can be filled with some dummy test data by running `add_test_data.py` in `scripts/`

## Testing

Bardic Scribbles has functional tests using pytest and selenium. It can be run as follows:

* Start an instance of the app (Database and frontend)
* Navigate to the `functional_tests` directory
* Create a pipenv shell with `pipenv shell`
* `pytest`

This should discover all functional tests and run them in a local Webdriver
