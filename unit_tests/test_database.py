import random
from app.database import Comment, form_comment_tree


def get_tree(comment: Comment) -> int:
    return len(comment.children) + sum([get_tree(child) for child in comment.children])


def test_form_comment_tree_returns_all_comments_passed_in():
    comments = [Comment(id=0)]
    for i in range(1, 500):
        comments.append(Comment(
            id=i,
            parent=random.choice(comments).id
        ))

    tree = form_comment_tree(comments)
    count = 1 + get_tree(tree[0])
    assert count == len(comments)
