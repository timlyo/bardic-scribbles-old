import os
import tempfile

import pytest

from website import app


@pytest.fixture
def client():
    db_fd, app.app.config['DATABASE'] = tempfile.mkstemp()
    app.app.config['TESTING'] = True
    client = app.app.test_client()

    yield client

    os.close(db_fd)
    os.unlink(app.app.config['DATABASE'])
